package fr.epsi.resellersapi.Controller;

import fr.epsi.resellersapi.Model.Role;
import fr.epsi.resellersapi.Service.AuthService;
import fr.epsi.resellersapi.Service.RoleService;
import fr.epsi.resellersapi.dto.LoginRequest;
import fr.epsi.resellersapi.dto.RegisterRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/resellers/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final RoleService roleService;

    @PostMapping("/signup")
    public void signup(@RequestBody RegisterRequest registerRequest) throws Exception {
        authService.signup(registerRequest);
    }

    @PostMapping("/role")
    public void addRole(@RequestBody Role role) throws Exception {
        roleService.saveOrUpdate(role);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody @Valid LoginRequest loginRequest) {
        try {
           String jwtToken = authService.login(loginRequest);
            return ResponseEntity.ok().header(HttpHeaders.AUTHORIZATION,"Bearer " + jwtToken).body("hellooo");
        } catch (BadCredentialsException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
