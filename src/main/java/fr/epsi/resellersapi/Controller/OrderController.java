package fr.epsi.resellersapi.Controller;


import fr.epsi.resellersapi.Exceptions.InvalidRequestException;
import fr.epsi.resellersapi.Model.Order;
import fr.epsi.resellersapi.Model.Product;
import fr.epsi.resellersapi.Service.OrderService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@OpenAPIDefinition(info = @Info(
        title = "Resellers API",
        description = "C'est une API pour les revendeurs de l'application PayeTonKawa"))
@RequestMapping("/resellers/orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("/")
    public ResponseEntity<Order> addOrder(@RequestBody Order order) {
        Order orderOutPut = orderService.addOrder(order);
        return new ResponseEntity<>(orderOutPut, HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<List<Order>> findAllOrders() {
        List<Order> result = orderService.getOrders();
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> findOrderById(@PathVariable String id) {
        Order order =  orderService.getOrderById(id);
        if (order == null) throw new ResourceNotFoundException("Order with id "+id+" not found!");
        return ResponseEntity.ok().body(order);
    }

    @PutMapping("/")
    public ResponseEntity<Order> updateOrder(@RequestBody Order order) {
        if (order == null || order.getId() == null) {
            throw new InvalidRequestException("Order or id must not be null!");
        }
        Order orderOutPut = orderService.updateOrder(order);
        return  ResponseEntity.ok(orderOutPut);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable String id) {
        orderService.deleteOrder(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("")
    public ResponseEntity<List<Order>> findOrdersByCustomerId(@RequestParam String customerId) {
        List<Order> result =  orderService.getOrdersByCustomerId(customerId);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/{customerId}/{orderId}")
    public ResponseEntity<Order> findOrderByCustomerIdAndOrderId(@PathVariable String customerId, @PathVariable String orderId) {
        Order order = orderService.getOrdersByCustomerIdAndOrderId(customerId, orderId);
        return ResponseEntity.ok().body(order);
    }

}
