package fr.epsi.resellersapi.Controller;


import fr.epsi.resellersapi.Exceptions.InvalidRequestException;
import fr.epsi.resellersapi.Model.Order;
import fr.epsi.resellersapi.Model.Product;
import fr.epsi.resellersapi.Service.OrderService;
import fr.epsi.resellersapi.Service.ProductService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@OpenAPIDefinition(info = @Info(
        title = "Resellers API",
        description = "C'est une API pour les revendeurs de l'application PayeTonKawa"))
@RequestMapping("/resellers/products")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderService orderService;
    @PostMapping("/")
    public ResponseEntity<Product> addProduct(@RequestBody Product product) {
        Product productOutPut = productService.addProduct(product);
        return new ResponseEntity<>(productOutPut, HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<List<Product>> findAllProducts() {
        List<Product> result = productService.getProducts();

        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findProductById(@PathVariable String id) {
        Product product = productService.getProductById(id);
        return ResponseEntity.ok().body(product);
    }

    @PutMapping
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
        Product productOutPut = productService.updateProduct(product);
        return  ResponseEntity.ok(productOutPut);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable String id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{customerId}/{orderId}/{productId}")
    public ResponseEntity<Product> findProductByCustomer(@PathVariable String customerId, @PathVariable String orderId, @PathVariable String productId) {
        System.out.println("CUSTOMERID : " + customerId + " OrderID " + orderId);
        Order order = orderService.getOrdersByCustomerIdAndOrderId(customerId, orderId);
        System.out.println("ORDER FOUND OR NO : " +order);
        Product product = productService.getProductByIdFromOrder(productId, order);
        return ResponseEntity.ok().body(product);
    }
}

