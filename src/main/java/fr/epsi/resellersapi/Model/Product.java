package fr.epsi.resellersapi.Model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "Product")
@Builder
public class Product {
    private @MongoId(FieldType.OBJECT_ID) String id;

    private String libelle;

    private String name;

    private Details details;

    private Long stock;

    @CreatedDate @Builder.Default
    private Date createdAt = new Date();

    @LastModifiedDate @Builder.Default
    private Date updatedAt = new Date();


    public Product(String libelle, String name, Details details, Long stock) {
        this.libelle = libelle;
        this.name = name;
        this.details = details;
        this.stock = stock;
    }
}
