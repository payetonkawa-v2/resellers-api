package fr.epsi.resellersapi.Repository;

import fr.epsi.resellersapi.Model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface OrderRepository extends MongoRepository<Order, String> {

    @Query("{'customerId' : ?0}")
    List<Order> findAllByCustomerId(String customerId);

    @Query("{$and:[ {'customerId' : ?0},{'id' : ?1} ]}")
    Order findOrderByCustomerIdAndOrderId(String customerId, String id);

}
