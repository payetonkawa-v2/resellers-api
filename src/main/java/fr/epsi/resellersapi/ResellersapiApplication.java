package fr.epsi.resellersapi;

import fr.epsi.resellersapi.Config.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResellersapiApplication {
	public static void main(String[] args) {
		SpringApplication.run(ResellersapiApplication.class, args);
	}
}
