package fr.epsi.resellersapi.Service;

import fr.epsi.resellersapi.Jwt.JwtProvider;
import fr.epsi.resellersapi.Model.NotificationEmail;
import fr.epsi.resellersapi.Model.QRCode;
import fr.epsi.resellersapi.Model.Role;
import fr.epsi.resellersapi.Model.User;
import fr.epsi.resellersapi.Repository.UserRepository;
import fr.epsi.resellersapi.dto.LoginRequest;
import fr.epsi.resellersapi.dto.RegisterRequest;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Base64;

@Service
@AllArgsConstructor
public class AuthService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final MailService mailService;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final QRCodeService qrCodeService;


    public void signup(RegisterRequest registerRequest) throws Exception {
        User user = new User();
        user.setEmail(registerRequest.getEmail());
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        addRoleToUser(user, "USER");
        user.setEnabled(true);

        userRepository.save(user);

        String token = jwtProvider.generateTokenQR(user);

        QRCode qrCode = new QRCode();
        qrCode.setToken(token);
        qrCodeService.save(qrCode);
        byte[] qrcode = qrCodeService.generateQRCode(token);

        mailService.sendMail(new NotificationEmail("Please Connecte to your Account with the QRCODE BELOW",
                user.getEmail(), "Thank you for signing up to paytonkawa, " +
                user.getEmail(), Base64.getEncoder().encodeToString(qrcode))
               );
    }

    /*
    @Transactional(readOnly = true)
    public User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        return userRepository.findByEmail(currentPrincipalName)
                .orElseThrow(() -> new RedditException("User email not found - " + currentPrincipalName));
    }*/

    /*
    public void addRoleToUser(String email, String roleName){
        log.info("Adding role {} to user {}", roleName, email);
        User user = userRepository.findByEmail(email).orElseThrow(() ->
                new RedditException("User not found with email - " + email));
        Role role = roleRepository.findByName(roleName).orElseThrow(() ->
                new RedditException("Role not found with name - " + roleName));
        user.getAutorities().add(role);
    }*/


    public String login(LoginRequest loginRequest) {
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                ));
        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) authentication.getPrincipal();

        return jwtProvider.generateToken(user);
    }

    private void addRoleToUser(User user, String roleName) throws Exception {
        Role role = roleService.getRole(roleName);
        user.getAutorities().add(role);
    }


    /*private String generateVerificationToken(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);

        verificationTokenRepository.save(verificationToken);
        return token;
    }*/

    public boolean isLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }

}
