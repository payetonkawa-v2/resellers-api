package fr.epsi.resellersapi.Service;

import fr.epsi.resellersapi.Exceptions.InvalidRequestException;
import fr.epsi.resellersapi.Model.Order;
import fr.epsi.resellersapi.Model.Product;
import fr.epsi.resellersapi.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public Order getOrderById(String id) {
        if (id == null || id.length() != 24) {
            throw new InvalidRequestException("Order ID  infos must not be null or in invalide format!");
        }
        if (orderRepository.findById(id).orElse(null) == null) throw new ResourceNotFoundException("Order  not found!");
        return orderRepository.findById(id).orElse(null);
    }

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public Order addOrder(Order order) {
        if (order == null || order.getCustomerId() == null || order.getProductsId() == null || order.getProductsId().isEmpty() ) {
            throw new InvalidRequestException("Order infos must not be null!");
        }
        return orderRepository.save(order);
    }

    public Order updateOrder(Order order) {
        if (order == null || order.getId() == null) {
            throw new InvalidRequestException("Order or id must not be null!");
        }
        Order existingOrder = orderRepository.findById(order.getId()).orElse(null);
        if (existingOrder == null) throw new ResourceNotFoundException("Order with id "+order.getId()+" not found!");
        existingOrder.setCustomerId(order.getCustomerId());
        existingOrder.setProductsId(order.getProductsId());
        return orderRepository.save(existingOrder);
    }

    public void deleteOrder(String id) {
        Order existingOrder = getOrderById(id);
        if (existingOrder == null) {
            throw new ResourceNotFoundException("Order with id " + id + " not found!");
        }
        orderRepository.deleteById(id);
    }

    public List<Order> getOrdersByCustomerId(String customerId) {
        if (customerId == null || customerId.length() != 24) {
            throw new InvalidRequestException("CustomerID ID  infos must not be null or in invalide format!");
        }
        if (orderRepository.findAllByCustomerId(customerId) == null) throw new ResourceNotFoundException("Order  not found!");
        return orderRepository.findAllByCustomerId(customerId);
    }

    public Order getOrdersByCustomerIdAndOrderId(String customerId, String orderId) {
        if (customerId == null || customerId.length() != 24 || orderId == null || orderId.length() != 24) {
            throw new InvalidRequestException("CustomerID ID OR Order ID infos must not be null or in invalide format!");
        }
        if (orderRepository.findOrderByCustomerIdAndOrderId(customerId, orderId) == null) throw new ResourceNotFoundException("Order  not found!");
        return orderRepository.findOrderByCustomerIdAndOrderId(customerId, orderId);
    }

}
