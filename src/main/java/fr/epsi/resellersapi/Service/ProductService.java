package fr.epsi.resellersapi.Service;

import java.util.Date;
import java.util.List;

import fr.epsi.resellersapi.Exceptions.InvalidRequestException;
import fr.epsi.resellersapi.Model.Order;
import fr.epsi.resellersapi.Model.Product;
import fr.epsi.resellersapi.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public Product getProductById(String id) {
        if (id == null || id.length() != 24) {
            throw new InvalidRequestException("Product ID  infos must not be null or in invalide format!");
        }
        if (productRepository.findById(id).orElse(null) == null) throw new ResourceNotFoundException("Product  not found!");

        return productRepository.findById(id).orElse(null);
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public Product addProduct(Product product) {
        if (product == null || product.getLibelle() == null || product.getName() == null || product.getDetails() == null || product.getStock() == null) {
            throw new InvalidRequestException("Product infos must not be null!");
        }
        return productRepository.save(product);
    }

    public Product updateProduct( Product product) {
        if (product == null || product.getId() == null) {
            throw new InvalidRequestException("Product or id must not be null!");
        }
        Product existingProduct = productRepository.findById(product.getId()).orElse(null);
        if (existingProduct == null) throw new ResourceNotFoundException("Product with id "+product.getId()+" not found!");
        existingProduct.setLibelle(product.getLibelle());
        existingProduct.setName(product.getName());
        existingProduct.setDetails(product.getDetails());
        existingProduct.setStock(product.getStock());
        existingProduct.setUpdatedAt(new Date());
        return productRepository.save(existingProduct);
    }

    public void deleteProduct(String id) {
        Product existingProduct = getProductById(id);
        if (existingProduct == null) {
            throw new ResourceNotFoundException("Product with id " + id + " not found!");
        }
        productRepository.deleteById(id);
    }

    public Product getProductByIdFromOrder(String productId, Order order) {
        Boolean exist = false;
        List<Product> productsList = order.getProductsId();
        Product product = new Product();
        for (Product element : productsList) {
            if(element.getId().equals(productId) ) {
                product = element;
                System.out.println(product);
                exist = true ;
                break;
            }
        }
        if (!exist) {
            throw new ResourceNotFoundException("Product with id "+productId+" not found!");
        }
        return product;
    }

}

