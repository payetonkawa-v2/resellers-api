package fr.epsi.resellersapi.exception;

public class ApiException extends RuntimeException {
    public ApiException(String exception) {
        super(exception);
    }
}
